var sliderFactory = {
    creetNewSlider: function() {
        var newSlider = {
            imagesUrls: [],
            curentImageIndex: 0,
            showPrevBtn: null, // document.getSelection('show-prev-btn'),
            showNextBtn: null, // document.getSelection('show-next-btn'),
            slideImage: null, // document.getSelection('slide-img'),
        
            start: function(elId) {
                var that = this;
        
                var elSelector = '#' + elId;
                var el = document.querySelector(elSelector);
                
                this.showPrevBtn = el.querySelector('.show-prev-btn');
                this.showNextBtn = el.querySelector('.show-next-btn');
                this.slideImage = el.querySelector('.slide-img');
        
                this.showPrevBtn.addEventListener('click', function(e) {
                    that.onshowPrevBtnClick(e);
                });
                this.showNextBtn.addEventListener('click', function(e) {
                    that.onshowNextBtnClick(e);
                });
        
                this.imagesUrls.push('https://i.pinimg.com/736x/1b/5a/9f/1b5a9f1b8d198546c0f66f245f402da7--left-nice-cars.jpg');
                this.imagesUrls.push('https://img.favcars.com/ferrari/f12berlinetta/ferrari_f12berlinetta_2012_images_1_800x600.jpg');
                this.imagesUrls.push('https://torno.lv/upl/114380/65756.jpg');
                this.imagesUrls.push('https://f.vividscreen.info/soft/8c64d37deea93cca710ffa9b4e7f891a/Dodge-Challenger-SRT8-800x600.jpg');
        
                this.slideImage.src = this.imagesUrls[this.curentImageIndex];
                this.showPrevBtn.disabled = true;
            },
                
            onshowPrevBtnClick: function (e) {
                 this.curentImageIndex--;
                 this.slideImage.src = this.imagesUrls[this.curentImageIndex];
                 this.showNextBtn.disabled = false;
                 if (this.curentImageIndex === 0) {
                     this.showPrevBtn.disabled = true;
                 }
            },
        
            onshowNextBtnClick: function (e) {
                 this.curentImageIndex++;
                 this.slideImage.src = this.imagesUrls[this.curentImageIndex];
                 this.showPrevBtn.disabled = false;
        
                 if (this.curentImageIndex === (this.imagesUrls.length - 1)) {
                     this.showNextBtn.disabled = true;
                 }
            }
        }   
        
        return newSlider;
    }
    
}

